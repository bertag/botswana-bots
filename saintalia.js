var SaintAlia = function() {};

SaintAlia.prototype = new Bot();

SaintAlia.prototype.setup = function() {
	this.timer = 0;
	this.attrStrength = 10;
	this.safety = 10;
	this.repStrength = 500;
	this.movements = ['strafe-right', 'strafe-left', 'forward', 'backward'];
	this.formation = this.id % 2;
	this.strafe = 100;
};

SaintAlia.prototype.getBotById = function(id) {
	for (i in this.state.bots) {
		if (this.state.bots[i].id == id) return this.state.bots[i];
	}
	return undefined;
};

SaintAlia.prototype.getOpponentBots = function() {
	op = [];
	for (i in this.state.bots) {
		if (this.name != this.state.bots[i].name) {
			op.push(this.state.bots[i]);
		}
	}
	return op;
};

SaintAlia.prototype.getTeamBots = function() {
	team = [];
	for (i in this.state.bots) {
		if (this.name == this.state.bots[i].name) {
			team.push(this.state.bots[i]);
		}
	}
	return team;
};

SaintAlia.prototype.avoidCircle = function(radius, x, y, repelStrength) {
	radius -= 5;
	var dist = this.myDistanceToPoint(x, y);
	var angle = server.helpers.normalizeAngle(server.helpers.angleToPoint(this.x, this.y, x, y));
	angle += (Math.PI / 2);
	var dx = 0;
	var dy = 0;

	if (dist <= radius) {
		dx = (-1.0 * Math.cos(angle)) * 100000000;
		dy = (-1.0 * Math.sin(angle)) * 100000000;
	} else if (dist > radius && dist <= (2 * this.radius) + radius) {
		dx = -1.0 * repelStrength * (2 * this.radius + radius - dist) * Math.cos(angle);
		dy = -1.0 * repelStrength * (2 * this.radius + radius - dist) * Math.sin(angle);
	}

	return [dx, dy];
};

SaintAlia.prototype.acquireTarget = function() {
	if (typeof this.state.payload.targets == 'undefined') {
		this.state.payload.targets = {}
	}
	target = this.getBotById(this.state.payload.targets[this.id]);
	if (target == undefined) {
		opp = this.getOpponentBots();
		closeness = 10000;
		for (i in opp) {
			dist = this.myDistanceToPoint(opp[i].x, opp[i].y);
			if (dist < closeness) {
				target = opp[i];
				closeness = dist;
			}
		}
	}
	this.state.payload.targets[this.id] = target.id;
	return target;
};

SaintAlia.prototype.avoidBullets = function() {
	var dx = 0;
	var dy = 0;
	for (i in this.state.weapons) {
		var bullet = this.state.weapons[i];
		if (bullet.owner != this.id && this.myDistanceToPoint(bullet.x, bullet.y) < 50) {
			deltas = this.avoidCircle(10, bullet.x, bullet.y, 25);
			dx += deltas[0];
			dy += deltas[1];
		}
	}
	return [dx, dy];
};

SaintAlia.prototype.avoidTeammates = function() {
	var dx = 0;
	var dy = 0;
	var team = this.getTeamBots();
	for (i in team) {
		var bot = team[i];
		dist = this.myDistanceToPoint(bot.x, bot.y);
		if (bot.id != this.id && dist < 20 + bot.radius + this.radius) {
			deltas = this.avoidCircle(bot.radius, bot.x, bot.y, 5);
			dx += deltas[0];
			dy += deltas[1];
		}
	}
	return [dx, dy];
};

SaintAlia.prototype.avoidObstacles = function() {
	var dx = 0;
	var dy = 0;
	var obstacles = this.state.obstacles;
	for (i in obstacles) {
		var obs = obstacles[i];
		dist = this.myDistanceToPoint(obs.x, obs.y);

		if (this.x >= obs.x - this.radius && this.x <= obs.x + this.radius + obs.width && this.y >= obs.y - this.radius && this.y <= obs.y + this.radius + obs.height) {
			deltas = this.avoidCircle(obs.radius, obs.x, obs.y, 5);
			dx += deltas[0];
			dy += deltas[1];
		}

/*
		if (obs.id != this.id && dist < 20 + obs.radius + this.radius) {
			deltas = this.avoidCircle(obs.radius, obs.x, obs.y, 5);
			dx += deltas[0];
			dy += deltas[1];
		}
*/
	}
	return [dx, dy];
}

SaintAlia.prototype.collisionObstacle = function(obstacle, point) {
		var rtnBool = false;

		if (point.radius != undefined) {
			// We have a bot
			if (point.x >= obstacle.x - point.radius && point.x <= obstacle.x + point.radius + obstacle.width && point.y >= obstacle.y - point.radius && point.y <= obstacle.y + point.radius + obstacle.height) {
				rtnBool = true;
			}
		} else {
			// Single point = bullet
			if (point.x >= obstacle.x && point.x <= obstacle.x + obstacle.width && point.y >= obstacle.y && point.y <= obstacle.y + obstacle.height) {
				rtnBool = true;
			}
		}

		return rtnBool;
	}

SaintAlia.prototype.run = function() {
	this.state.world = {
		'width': 1500,
		'height': 900,
	}

	this.timer++;
	target = undefined;

	var rtnCommand = 'wait';
	// get the opponent's information
	target = this.acquireTarget();

	// have I collided with a bot?
	this.collided = false;
	if (this.collisions.length > 0) {
		for (i in this.collisions) {
			if (this.collisions[i].type != 'bot')
				this.collided = true;
		}
	}

	var avoidBullets = this.avoidBullets();
	var avoidMates = this.avoidTeammates();
	var avoid = [avoidBullets[0] + avoidMates[0], avoidBullets[1] + avoidMates[1]]
	var avoidDir = this.getDirection({'x': avoid[0], 'y': avoid[1]}, 0.01);

	// if I need to avoid bullets or teammates, strafe-left or right
	if (avoid[0] != 0 && avoid[1] != 0 && avoidDir.command != 'forward' && avoidDir.command != 'wait') {
		var behindMe = this.angle - Math.PI;
		if (avoidDir.command == 'right')
			rtnCommand = 'strafe-right';
		else
			rtnCommand = 'strafe-left';
	}

	//not a formation 1 bot and have a target
	if (target != undefined) {
		dir = this.getDirection(target, 0.05);
		dist = this.myDistanceToPoint(target.x, target.y);

		if(dist < 50 && this.health < 8) {
			rtnCommand = "self-destruct";
		}
		if (dir.command != 'forward') {
			// if target is not in front of me, do what it takes to get there.
			rtnCommand = dir.command;
		
/*
		} else if (dist > 10 * this.radius) {
			rtnCommand = "forward";
*/
		} else if (this.canShoot && this.weapons.bullet > 0) {
			// if the target is in front and I can show and I have bullets to shoot, shoot!
			rtnCommand = "fire";
		} else {
			if(this.health < target.health / 2) {
				if(this.canShoot && this.weapons.mine > 0) {
					rtnCommand = "mine";
				} else {
					rtnCommand = "backward";
				}
			} else {
				rtnCommand = "forward";
			}
		}
/*
		} else if (target.health <= this.health) {
			// if the targets health is less than my own, attack!
			rtnCommand = "forward";
		} else if (rtnCommand == 'wait' && dist < 150 && target.health > this.health) {
			// move backwards, if the target is close enough and it's health is greater than my own.
			rtnCommand = "backward";
		}
*/
	} 

	// just move backwards every now and then
	if (rtnCommand != 'fire' && this.timer % 30 == 0)
		rtnCommand = 'backward';

	return {'command': rtnCommand, 'team': this.state.payload};
};


SaintAlia.prototype.getDirection = function(target, threshold) {
		// Make sure the target is real
		if (target == undefined) {
				return { 'command': 'error', 'angle': 0 };
		}

		var targetAngle = this.angle;
		var dist = this.myDistanceToPoint(target.x, target.y);
		var angle = server.helpers.normalizeAngle(server.helpers.angleToPoint(this.x, this.y, target.x, target.y));
		var strength = (typeof this.attrStrength !== 'undefined') ? this.attrStrength : 5;
		var repelStrength = (typeof this.repStrength !== 'undefined') ? this.repStrength : 2;
		var threshold = (typeof threshold !== 'undefined') ? threshold : 0.05;
		var dx = 0;
		var dy = 0;

		if (typeof this.safety === 'undefined') {
				this.safety = 7;
		}

		// Attractive potential field to target
		if (this.radius <= dist && dist <= this.safety * this.radius) {
				dx = strength * (dist - this.radius) * Math.cos(angle);
				dy = strength * (dist - this.radius) * Math.sin(angle);
		} else if (dist > this.safety * this.radius) {
				dx = strength * this.safety * Math.cos(angle);
				dy = strength * this.safety * Math.sin(angle);
		}

		// Repulsive potential field from obstacles
		var obstacles = server.getObstacles();
		obstacles.push({ x: 0, y: 0, width: this.radius * 2, height: this.state.world.height }), //left of arena
		//obstacles.push({ x: 0, y: 0, width: this.state.world.width, height: this.radius * 2}), //top of arena
		obstacles.push({ x: this.state.world.width - this.radius * 2, y: 0, width: this.radius * 2 , height: this.state.world.height }), //right of arena
		//obstacles.push({ x: 0, y: this.state.world.height - this.radius * 2, width: this.state.world.width, height: this.radius * 2}), //bottom of arena
		server.miniObstacles = [];

		for (i in obstacles) {
				var obs = obstacles[i];

				if ((obs.x != undefined && obs.y != undefined && obs.radius != undefined) || (obs.width == obs.height)) {
						// It's a circle type object
						var middleX = obs.x + Math.ceil(obs.width / 2);
						var middleY = obs.y + Math.ceil(obs.width / 2);

						radius = this.distanceToPoint(obs.x, obs.y, middleX , middleY) + 3;

						server.miniObstacles.push({ 'x': middleX, 'y': middleY, 'radius': radius });

						dxdy = this.avoidCircle(radius, middleX, middleY, repelStrength);

						dx += dxdy[0];
						dy += dxdy[1];
				} else if (obs.x != undefined && obs.y != undefined && obs.width != undefined && obs.height != undefined) {
						// It's a four-sided thing

						// Take the shortest dimension, halve it, and divide the larger dimension by that number.
						// That's the number of circles to draw along the larger dimension. Do the repulsion field stuff from those obstacles.

						var middle = Math.ceil(obs.width / 2);
						var radius = this.distanceToPoint(obs.x, obs.y, obs.x + middle, obs.y + middle) + 3;
						var propagate = 'height';
						var numMiniObs = Math.ceil(obs.height / middle);

						if (Math.ceil(obs.height / 2) < obs.width) {
								middle = Math.ceil(obs.height / 2);
								radius = this.distanceToPoint(obs.x, obs.y, obs.x + middle, obs.y + middle) + 3;
								propagate = 'width';
								numMiniObs = Math.ceil(obs.width / middle);
						}

						numMiniObs -= 2;

						for (i=0; i<=numMiniObs; i++) {
								var x = obs.x + middle;
								var y = obs.y + (middle * (i+1));

								if (propagate == 'width') {
										x = obs.x + (middle * (i+1));
										y = obs.y + middle;
								}

								server.miniObstacles.push({ 'x': x, 'y': y, 'radius': radius });

								dxdy = this.avoidCircle(radius, x, y, repelStrength);

								dx += dxdy[0];
								dy += dxdy[1];
						}
				} else { 
						// Something else entirely
						console.log("Shouldn't be here");
				}
		}

		// Was there a change in my location?
		if (dx != 0 || dy != 0) {
				targetAngle = server.helpers.normalizeAngle(server.helpers.angleToPoint(this.x, this.y, this.x + dx, this.y + dy));
		}

		targetAngle = server.helpers.normalizeAngle(targetAngle - this.angle);

		var command = 'right';

		if (targetAngle > Math.PI) {
				command = 'left';
		}

		if (targetAngle <= threshold || targetAngle >= ((2 * Math.PI) - threshold)) {
				command = "forward";
		}

		return { 'command': command, 'angle': targetAngle };
};

server.registerBotScript("SaintAlia");
